require 'aws-sdk'
require 'json'

@iam = Aws::IAM::Client.new

@fullList = @iam.get_account_authorization_details({
  filter: ["User"]
})

def storeNameAndKeys(userNameInput)
  keyFromName = @iam.list_access_keys({ user_name: userNameInput })
  awsKey = keyFromName.to_s.scan(/(?<=access_key_id=").*?(?=\")/)
  nameAndAccessKeys = Hash.new {|key, value| key[value] = []}
  nameAndAccessKeys[userNameInput] << awsKey
  puts JSON.pretty_generate nameAndAccessKeys
end

def userList
    maxUsers = 0
  while maxUsers
    begin
    name = @fullList.user_detail_list[maxUsers].user_name
    storeNameAndKeys(name)
    maxUsers = maxUsers + 1
    rescue Exception
      exit
    end
  end
end

userList