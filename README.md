# AWS-User-And-Key-JSON
Displays a JSON list of all AWS user and access key IDs


Necessary ruby gems: aws-sdk and json

Run:
gem install aws-sdk json


To run, input your aws access key ID, secret access key,
and desired region as below.

The following example is designed for the fish shell:

env AWS_ACCESS_KEY_ID=yourAccessKeyGoesHere \
AWS_SECRET_ACCESS_KEY=yourSecretKeyGoesHere \
AWS_DEFAULT_REGION=yourDesiredRegionGoesHere \
ruby listUsersAndKeys.rb